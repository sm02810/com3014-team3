from django.core.mail import send_mail, BadHeaderError, EmailMessage
from django.http import HttpResponse, HttpResponseRedirect 
from django.shortcuts import render, redirect 
from django.urls import reverse, reverse_lazy
from django.contrib import messages
from django.views.generic import CreateView, ListView, DetailView
from .forms import addPostForm
from django.shortcuts import (get_object_or_404, render, redirect)
from .models import post, Comment
from django.contrib.auth.decorators import login_required
from django.conf import settings
User = settings.AUTH_USER_MODEL
from django.core.exceptions import PermissionDenied
from django.contrib.auth.mixins import LoginRequiredMixin
from .forms import CommentForm
from django.core.files.base import ContentFile
import os
from django.core.files.storage import default_storage
from django.http import JsonResponse

@login_required
def newPostView(request):
  context = {}
  #Your mystery field was actually the image not uploading
  #just needed request.FILES adding in
  form = addPostForm(request.POST, request.FILES or None)
  if(request.method == 'POST'):
    if form.is_valid():
      #user wasnt being saved properly either 
      #gets values from form
      post = form.save(commit=False)
      #assigns current user as author
      post.author = request.user
      #saves post
      post.save()
      messages.add_message(request, messages.SUCCESS, 'Post Created')
      return redirect('post_index')
    else:
      messages.add_message(request, messages.ERROR, 'Invalid form data; post not created')
  context['form'] = form
  return render(request, "post/newpostpage.html", context)


class postDetailView(DetailView):
    model = post
    template_name = 'post/postdetail.html'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['comment_form'] = CommentForm()
        return context

    def post(self, request, *args, **kwargs):
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.post = self.get_object()
            comment.save()
            return redirect('post_detail', pk=self.kwargs['pk'])
        else:
            return redirect('post_detail', pk=self.kwargs['pk'])

def delete_comment(request, comment_id):
    try:
        comment = Comment.objects.get(pk=comment_id)
        comment.delete()
        return JsonResponse({'success': True})
    except Comment.DoesNotExist:
        return JsonResponse({'success': False, 'error': 'Comment does not exist'})

@login_required
def index_view(request): 
    context = {} 
    # Get all posts
    if request.user.is_staff:
        all_posts = post.objects.all()
    else:
        all_posts = post.objects.filter(author=request.user)
    # Filter out posts without a valid image
    valid_posts = []
    for post_item in all_posts:
        if post_item.image and default_storage.exists(post_item.image.name):
            valid_posts.append(post_item)
    context["post_list"] = valid_posts
    return render(request, "post/postIndex.html", context)

def feed_view(request):
  all_posts = sorted(post.objects.all(), key=lambda p:(p.created_at), reverse=True)
  valid_posts = []
  for post_item in all_posts:
      if post_item.image and default_storage.exists(post_item.image.name):
          valid_posts.append(post_item)
  return render(request, "post/postIndex.html", {'post_list':valid_posts})

@login_required
def delete_view(request, nid):
  obj= get_object_or_404(post, id=nid)
  if(obj.author != request.user and not(is_admin(request.user))):
    raise PermissionDenied()
  obj.delete()
  messages.add_message(request, messages.SUCCESS, 'Post Deleted')
  return redirect('post_index')

@login_required
def add_comment_view(request, pk):
    post_object = get_object_or_404(post, pk=pk)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.author = request.user
            comment.post = post_object
            comment.save()
            return redirect('post_detail', pk=pk)
    else:
        form = CommentForm()
    # Redirect to post detail page
    return redirect('post_detail', pk=pk)  
