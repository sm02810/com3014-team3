from django.db import models
from django.core.validators import RegexValidator
from django.conf import settings


alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', '') #Just in case 

class post(models.Model):
    #login profile
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True) #Needs improvement, compare to others
    description = models.TextField()
    created_at = models.DateTimeField(auto_now = True) #Simple enough
    updated_at = models.DateTimeField(auto_now=True) 
    treats = models.IntegerField(default=0) #our 'likes' counter. must start at 0
    # image = models.ImageField(upload_to='uploads/')
    image=models.ImageField(upload_to='img')


    def __str__(self):
      #just needed to add this lil format thing here
      return "{}".format(self.id)
    # class Meta:
    #     indexes= [models.Index(fields=['id'])]

class Comment(models.Model):
    post = models.ForeignKey(post, on_delete=models.CASCADE, related_name='comments', default=1)  # Provide a default value
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.SET_NULL, null=True, blank=True)
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)