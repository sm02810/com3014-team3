from django import forms
from .models import post
from .models import *
from django.db import models

class addPostForm(forms.ModelForm):
  class Meta:
    #The form for creating a new post
    model = post
    fields = ['image', 'description']

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['content']