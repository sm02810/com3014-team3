from django.urls import include, path
from . import views
from .views import post

urlpatterns = [ 
    path('newPost', views.newPostView, name='newPost'), #Page for making a new post
    path('<int:pk>/', views.postDetailView.as_view(), name='post_detail'), # specific post
    path('delete/<int:nid>', views.delete_view, name = 'post_delete'), #to delete a  post
    path('', views.index_view, name='post_index'),
    path('feed', views.feed_view, name='feed'),
    path('newPost/', views.newPostView, name='newPost'),
    path('<int:pk>/add_comment/', views.add_comment_view, name='add_comment'),
     path('delete-comment/<int:comment_id>/', views.delete_comment, name='delete_comment'),
]
