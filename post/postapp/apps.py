from django.apps import AppConfig


class PostappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'postapp'

class PostConfig(PostappConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'post.postapp'
