from django.shortcuts import render

def about(request):
    # opens the about page
    return render(request, 'about/about.html', {})