from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.contact, name='contact'),
    path('contact', views.contact, name='contact'),
]