from django.shortcuts import render, redirect 
from .forms import ContactForm 

def contact(request):
  if request.method == "POST":
    # specifies teh form
    form = ContactForm(request.POST) 
    #checks if its valid
    if form.is_valid(): 
      #saves the form
      form.save()
      #redirects to welcome page
      return redirect('http://localhost:8000/register/welcome')
  else:
    form = ContactForm()
  #opens contact page with form
  return render(request, 'contact/contactpage.html', {'form': form})