from django.db import models

class Contact(models.Model):
    name = models.CharField(max_length = 128)
    email = models.EmailField()
    subject = models.CharField(max_length = 256)
    message = models.TextField()
    sent_at = models.DateTimeField(auto_now = True)
    def __str__(self):
        return f"{self.subject}"