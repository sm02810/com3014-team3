from django.shortcuts import render, redirect
from .forms import RegistrationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm


def register(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()

            # After successful registration, authenticate the user and redirect to the login page
            username = form.cleaned_data['username']
            password = form.cleaned_data['password1']  # Assuming your form has password1 and password2 for password fields
            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('login')

    else:
        form = RegistrationForm()

    return render(request, 'register/register.html', {'form': form})


def home(request):
    return render(request, 'register/home.html')


def login_view(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            #saving user data
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                messages.success(request, 'Login successful.')
                return redirect('welcome')  # Redirect to the welcome page after login
            else:
                messages.error(request, 'Invalid username or password.')
        else:
            messages.error(request, 'Invalid username or password.')

    form = AuthenticationForm()
    return render(request, 'register/login.html', {'form': form})


def welcome(request):
    # Assuming you have a way to get the logged-in user
    username = request.user.username  # Change this according to your user model

    context = {'username': username}
    return render(request, 'register/welcome.html', context)