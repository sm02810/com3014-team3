from django.apps import AppConfig

class RegisterappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'registerapp'

class RegisterConfig(RegisterappConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'register.registerapp'
