from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Profile
from .forms import ProfileForm
from PIL import Image
from post.postapp.models import post
from post.postapp.views import index_view
from django.core.files.storage import default_storage

@login_required
def view_profile(request):
    profile = Profile.objects.get_or_create(user=request.user)[0]
    # Resize profile picture if it exists
    if profile.profile_picture:
        # Open the image
        image = Image.open(profile.profile_picture.path)
        
        # Define maximum size
        max_size = (200, 200)
        
        # Resize image
        image.thumbnail(max_size)
        
        # Save resized image (overwrite the original)
        image.save(profile.profile_picture.path)
    # Get all posts
    if request.user.is_staff:
        all_posts = post.objects.all()
    else:
        all_posts = post.objects.filter(author=request.user)
    # Filter out posts without a valid image
    valid_posts = []
    for post_item in all_posts:
        if post_item.image and default_storage.exists(post_item.image.name):
            valid_posts.append(post_item)
    return render(request, 'profile/view_profile.html', {'profile': profile, 'post_list': valid_posts})
    


@login_required
def edit_profile(request):
    profile = request.user.profile

    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES, instance=profile)
        if form.is_valid():
            request.user.username = form.cleaned_data['username']
            request.user.save()
            form.save()
            return redirect('view_profile')
    else:
        # Initialize the form with the current username
        form = ProfileForm(instance=profile, initial={'username': request.user.username})
    
    return render(request, 'profile/edit_profile.html', {'form': form})
