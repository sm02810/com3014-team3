from django.contrib import admin
from django.urls import path, include
# from register.views import register, home
from profileapp.views import edit_profile, view_profile
from . import views

urlpatterns = [
    path('', view_profile, name='view_profile'),
    path('view/<str:username>', view_profile, name='view_profile'),
    path('edit', edit_profile, name='edit_profile'),
    # path('user/<str:username>/', views.user_posts, name='user_posts'),
]