from django import forms
from .models import Profile

class ProfileForm(forms.ModelForm):
    username = forms.CharField(max_length=150, required=True)
    
    class Meta:
        model = Profile
        fields = ['bio', 'profile_picture']  # Add more fields as needed